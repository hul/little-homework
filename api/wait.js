const wait = miliseconds => {
  const start = new Date().getTime();
  let elapsed;
  do {
    elapsed = new Date().getTime() - start;
  } while (elapsed < miliseconds);
};

module.exports = wait;