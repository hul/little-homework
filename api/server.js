const express = require('express');
const bodyParser = require('body-parser');
const wait = require('./wait');
const app = express();
const cors = require('cors');

const persons = [];

app.use(bodyParser.json());

app.use(cors());

app.options('*', cors())

app.get('/persons', function (req, res) {
  res.json(persons);
});

app.post('/persons', function(req, res) {
  wait(1000);
  const person = Object.assign(req.body, { 
    id: persons.length + 1,
  });
  persons.push(person);
  res.json(person);
});

app.patch('/person/:personId', function(req, res) {
  const person = persons.find(person => person.id === parseInt(req.params.personId));
  if (person) {
    Object.assign(person, req.body);
    res.json(person);
  } else {
    res.status(404).json({ error: 'person not found' });
  }
});

app.listen(3000, () => console.log('Ovos Little Homework API is listening on port 3000!'));
