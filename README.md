# Ovos Little Homework

UI - Server syncing

### Prerequisites

git, node

### Installing

In the terminal/command line do the following:

```
git clone https://hul@bitbucket.org/hul/little-homework.git
cd little-homework
npm i 
npm run start
```
and open the [localhost](http://localhost:8080/) with your favourite browser.

## About

### Server

api/server.js is a simple express application that provides following endpoints:

GET /persons
POST /person
PATCH /person

To illustrate the issue of UI Server syncing POST /person delays the response with usage of `wait(duration)` helper function.

The server can be launched separately with 
`node server.js`
or in parallel with http-sever with
`npm run start`.

### Client

The issue of ui-server syncing occurs when client sends multiple POSTs requests which leads to invalid state of the data. In this solution it is not allowed to post anything if there are any ongoing requests.

`app/form.js` defines a post stream that emits only if these criteria are met:
 - model has no id (it has not been persisted yet),
 - no request is pending.
Filter operator checks these conditions and allows POST to be send.
```
  function initPost() {
    return submit$
      .pipe(
        filter(() => !model.isPersisted() && !progress$.value),
        tap(() => progress$.next(true)),
        flatMap(() => saveForm(http.create))
      );
  }
```

I wanted to provide generic solution for the issue, so I have decided to split the app into small pieces:

 - `index.js` is a main file, that creates and configures all other parts,
 - `config.js` contains two configuration options. First one is for the person form, second one is for second form, with slightly different structure. Currently this solution supports forms with text inputs only, but it could be easily extended to other types of input as well.
 - `app/model.js` defines the model, knows if model is persisted and allows to mutate it,
 - `app/list.js` fetches the data and render it as a unordered list, it can be customised by providing `itemRenderer`,
 - `app/form.js` could be broken down at least to two parts: form and form analysis (`reduceForm`, `inputToModelReducer`, `inputToStreamReducer`). This component handles the DOM events, decides wether to post, patch or do nothing. The form is submited after user click on Save button, or after it stops to type to any input.
 - `app/http.js` is responsible for http communication and allows to use the server api.
