const API_URL = 'http://127.0.0.1:3000/';

export const personConfig = {
  urls: {
    get: `${API_URL}persons`,
    post: `${API_URL}persons`,
    patch: `${API_URL}person/:id`,
  },
  listElement: document.getElementById('person-list'),
  formElement: document.getElementById('person-form'),
  modelFactory: () => ({
    name: '',
    id: -1
  }),
  itemRenderer: item => `<li>${item.id}. ${item.name}</li>`,
}

export const superPersonConfig = {
  urls: {
    get: `${API_URL}persons`,
    post: `${API_URL}persons`,
    patch: `${API_URL}person/:id`,
  },
  listElement: document.getElementById('superperson-list'),
  formElement: document.getElementById('superperson-form'),
  modelFactory: () => ({
    name: '',
    power: '',
    id: -1
  }),
  itemRenderer: item => `<li>${item.id}. ${item.name} has <b>${item.power}</b></li>`
}