export function createModel(modelFactory) {
  let model = modelFactory();

  function setModel(response) {
    return Object.assign(model, response);
  }
  
  function isPersisted() {
    return model.id !== -1;
  }

  return {
    model,
    isPersisted,
    setModel
  }
}
