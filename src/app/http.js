import { map, tap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

const HEADERS = { 
  'Content-Type': 'application/json' 
};

export function createHttp(config) {
  function create(model) {
    return ajax.post(config.post, model, HEADERS)
      .pipe(map(event => event.response));
  }
  
  function update(model) {
    return ajax.patch(config.patch.replace(':id', model.id), model, HEADERS)
      .pipe(map(event => event.response));
  }

  function fetchData(renderListItem) {
    return ajax.get(config.get)
      .pipe(map(ajaxResponse => ajaxResponse.response));
  }

  return {
    create, update, fetchData
  }
}
