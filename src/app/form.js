import { fromEvent, merge, Subject, BehaviorSubject } from 'rxjs';
import { flatMap, map, tap, filter, debounceTime, distinctUntilChanged } from 'rxjs/operators';

const DEBOUNCE_DURATION = 500;

function inputToStreamReducer(acc, curr) {
  const input$ = fromEvent(curr, 'input');
  return acc ? merge(acc, input$) : input$;
}

function inputToModelReducer(acc, curr) {
  const key = curr.getAttribute('name');
  acc[key] = curr.value;
  return acc;
}

function reduceForm(formElement, reducer, initialValue) {
  return Array.prototype.slice.call(formElement.elements)
    .filter(element => element.nodeName.toLowerCase() === 'input' && element.hasAttribute('name'))
    .reduce(reducer, initialValue);
}

export function createForm(config) {
  const { formElement, http, model } = config;
  const submit$ = fromEvent(formElement, 'submit');
  const input$ = reduceForm(formElement, inputToStreamReducer);    
  const progress$ = new BehaviorSubject(false);
  const refresh$ = new Subject();

  function saveForm(httpMethod) {
    model.setModel(Object.assign(reduceForm(formElement, inputToModelReducer, {})));
    return httpMethod(model.model);
  }

  function onSaved(response) {
    model.setModel(response);
    progress$.next(false);
    refresh$.next();
  }

  function initPost() {
    return submit$
      .pipe(
        filter(() => !model.isPersisted() && !progress$.value),
        tap(() => progress$.next(true)),
        flatMap(() => saveForm(http.create))
      );
  }

  function initPatch() {
    return submit$
      .pipe(
        filter(() => model.isPersisted() && !progress$.value),
        tap(() => progress$.next(true)),
        flatMap(() => saveForm(http.update))
      );
  }

  function initInput() {
    input$
      .pipe(
        debounceTime(DEBOUNCE_DURATION),
        distinctUntilChanged(),
        tap(() => formElement.submit.click())
      ).subscribe();
  }

  function preventDefault() {
    submit$
      .pipe(tap(event => event.preventDefault()))
      .subscribe();
  }

  preventDefault();
  initInput();

  const saved$ = merge(initPost(), initPatch());
  saved$
    .subscribe(onSaved);

  return {
    refresh$
  }
}
