import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { fetchData } from './http';

export function createList(http, listElement, itemRenderer) {
  const refresh$ = new Subject();

  function onRefresh() {
    http.fetchData()
      .pipe(
        map(response => response.map(itemRenderer).join('')),
        tap(html => listElement.innerHTML = html)
      ).subscribe();
  }

  refresh$
    .subscribe(onRefresh);

  return {
    refresh: () => refresh$.next()
  };
}
