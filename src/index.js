import { createList } from './app/list';
import { createHttp } from './app/http';
import { createModel } from './app/model';
import { createForm } from './app/form';
import { personConfig, superPersonConfig } from './config';

function main(config) {
  const { urls, formElement, listElement, modelFactory, itemRenderer } = config; 

  const model = createModel(modelFactory);
  const http = createHttp(urls); 
  const list = createList(http, listElement, itemRenderer);
  const form = createForm({ formElement, http, model });

  form.refresh$
    .subscribe(list.refresh);
  
  list.refresh();
}

main(personConfig);
main(superPersonConfig);